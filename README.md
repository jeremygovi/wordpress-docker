# Nginx + Let's Encrypt + php7 + postfix + Mariadb + Wordpress

A complete wordpress stack

<div align="center">
  <img src="https://mattgeri.com/wp-content/uploads/2015/12/wordpress-nginx-lets-encrypt.jpg" width="600"/>   
</div>

## Require:
- git (sudo apt-get install git)
- wget (sudo apt-get install wget)
- unzip (sudo apt-get install unzip)
- docker : https://docs.docker.com/engine/installation/
- docker-compose : can be installed following this link: https://docs.docker.com/compose/install/
- A server reachable from Internet (ports 80 and 443 must not be used by another process) and a DNS entry (or multiple) pointing to you server


## To use it:

### Clone this repo 

```
cd /a/path/of/your/server
git clone https://gitlab.com/jeremygovi/wordpress-docker.git

```

### Setup dropbox to obtain credentials
- First, create an app and generate token visiting the following URL (you should be logged in):
https://www.dropbox.com/developers/apps
- Then, clic on "generate token", get it and put it on your homedir in a file named ".dropbox_uploader"

```
cat ~/.dropbox_uploader 
OAUTH_ACCESS_TOKEN=<TheTokenYouJustGenerated>

```

### Prepare your configuration 

Edit "[docker-compose.yml](https://gitlab.com/jeremygovi/wordpress-docker/blob/master/docker-compose.yml)" and change the following variables according to your needs:

#### nginx-letsencrypt:
- ``SSL_VHOST_1``: set your main domain. Ex: jeremygovi.fr
- ``SSL_VHOST_1_ALT``: set your secondary domain. Ex: www.jeremygovi.fr, mobile.jeremygovi.fr. You can let the variable unset too
- ``SSL_VHOST_1_EMAIL``: Your email (mandatory). This email will be used by let's encrypt to notify you if a domain is about to expire 

#### postfix
- ``maildomain``: The maildomain. Ex: mail.jeremygovi.fr. It's used when talking with other mail servers
- ``smtp_user``: smtp user + password separated by ":". Ex: contact:azerty. It is used by wordpress to send emails (You need to configure this part in the wordpress itself later and provide the user/password you provided here)

/!\ The hostname (mailserver to connect to) from the wordpress will be "postfix" /!\

#### db:
- ``MYSQL_DATABASE``: set the database name you want to create for your wordpress. Ex: "wordpress"
- ``MYSQL_USER``: the username for the database. Ex: "wordpress"
- ``MYSQL_PASSWORD``: the password for your user. Please, provide a strong password
- ``MYSQL_ROOT_PASSWORD``: the root password. Please provide a very strong password 

#### dropbox-wordpress-backup
- ``$HOME/.dropbox_uploader:/root/.dropbox_uploader:ro``: Replace the first part of the volume ($HOME/.dropbox_uploader) with the path of the dropbox_uploader credentials you have configured on previous part


### Download wordpress

Get the latest version of wordpress in the official website

Put uncompressed files in the ``www`` dir of the project and rename directory with the name of your site
Example: 

```
cd ./www/
wget https://fr.wordpress.org/wordpress-4.7.1-fr_FR.zip
unzip wordpress-4.7.1-fr_FR.zip && rm wordpress-4.7.1-fr_FR.zip
mv wordpress jeremygovi.fr

```


### Create/Run the containers 


#### Create

```
[sudo] docker-compose create 

```

#### Run

```
[sudo] docker-compose start 

```


### Then ?

- See if there are no errors:

```
[sudo] docker-compose logs 

```

- Open your browser: https://yourdomain.com and follow the instruction for the wordpress installation. Provide the same information you provide on your [docker-compose.yml](https://gitlab.com/jeremygovi/wordpress-docker/blob/master/docker-compose.yml)
/!\ The DB hostname (database to connect to) from the wordpress will be "db" /!\

### Misc

- logs are persisted on ./logs
- certs are persisted on ./certs
- DB data are persisted on ./mysql/data

